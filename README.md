# Car Vertical


endpoints:
    
    Car Manufacturer - http://ec2-52-26-198-61.us-west-2.compute.amazonaws.com:5000/api/make
    Car Model - http://ec2-52-26-198-61.us-west-2.compute.amazonaws.com:5000/api/car_model 
    Car - http://ec2-52-26-198-61.us-west-2.compute.amazonaws.com:5000/api/car 
    Sale Order - http://ec2-52-26-198-61.us-west-2.compute.amazonaws.com:5000/api/sale
    Buyer - http://ec2-52-26-198-61.us-west-2.compute.amazonaws.com:5000/api/buyer 


how to run:

    docker pull povilaszva/carvertical
    docker run povilaszva/carvertical