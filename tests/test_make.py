import unittest
import json
import sys
sys.path.append('../')
from Model import db
from run import create_app

app = create_app("test_config")

class MakeTestCase(unittest.TestCase):
    """This class represents the Make test case"""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = app
        self.app.config['TESTING'] = True
        self.client = self.app.test_client
        self.make = {'name': 'TESTAS'}
        db.init_app(self.app)

        # binds the app to the current context
        with self.app.app_context():
            # create all tables
            db.create_all()

    def test_make_creation(self):
        """Test API can create a make (POST request)"""
        res = self.client().post('api/make', data=json.dumps(self.make))
        self.assertEqual(res.status_code, 201)
        self.assertIn('TESTAS', str(res.data))

    def test_api_can_get_all_make(self):
        """Test API can get a make (GET request)."""
        res = self.client().post('/api/make', data=json.dumps(self.make))
        self.assertEqual(res.status_code, 201)
        res = self.client().get('/api/make')
        self.assertEqual(res.status_code, 200)
        self.assertIn('TESTAS', str(res.data))

    def test_make_can_be_edited(self):
        """Test API can edit an existing make. (PUT request)"""
        rv = self.client().post(
            '/api/make',
            data=json.dumps(self.make))
        self.assertEqual(rv.status_code, 201)
        rv = self.client().put(
            '/api/make',
            data=json.dumps({"id": 1, "name": "updated"})
        )
        self.assertEqual(rv.status_code, 204)
        results = self.client().get('/api/make')
        self.assertIn('updated', str(results.data))

    def test_make_deletion(self):
        """Test API can delete an existing make. (DELETE request)."""
        rv = self.client().post('/api/make', data=json.dumps(self.make))
        res = self.client().delete('/api/make', data=json.dumps({'id': 1, 'name': 'TESTAS'}))
        self.assertEqual(res.status_code, 204)
        result = self.client().get('/api/make')
        self.assertNotIn('TESTAS', str(res.data))


    def tearDown(self):
        """teardown all initialized variables."""
        with self.app.app_context():
            # drop all tables
            db.session.remove()
            db.drop_all()


# Make the tests conveniently executable
if __name__ == "__main__":
    unittest.main()