from flask import request
from flask_restful import Resource
from Model import db, SaleOrder, SaleOrderSchema

sale_orders_schema = SaleOrderSchema(many=True)
sale_order_schema = SaleOrderSchema()

class SaleOrderResource(Resource):
    def get(self):
        sale_orders = SaleOrder.query.all()
        sale_orders = sale_orders_schema.dump(sale_orders).data
        return {'status': 'success', 'data': sale_orders}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = sale_order_schema.load(json_data)
        if errors:
            return errors, 422
        sale_order = SaleOrder.query.filter_by(name=data['name']).first()
        if sale_order:
            return {'message': 'Sale Order already exists'}, 400
        sale_order = SaleOrder(
            name=data['name'],
            car_id=data['car_id'],
            buyer_id=data['buyer_id'],
            date_sold=data['date_sold'],
            )

        db.session.add(sale_order)
        db.session.commit()

        result = sale_order_schema.dump(sale_order).data

        return { "status": 'success', 'data': result }, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = sale_order_schema.load(json_data)
        if errors:
            return errors, 422
        sale_order = SaleOrder.query.filter_by(id=data['id']).first()
        if not sale_order:
            return {'message': 'Sale Order does not exist'}, 400
        sale_order.name = data['name']
        db.session.commit()

        result = sale_order_schema.dump(sale_order).data

        return { "status": 'success', 'data': result }, 204

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = sale_order_schema.load(json_data)
        if errors:
            return errors, 422
        sale_order = SaleOrder.query.filter_by(id=data['id']).delete()
        db.session.commit()

        result = sale_order_schema.dump(sale_order).data

        return { "status": 'success', 'data': result}, 204
