from flask import request
from flask_restful import Resource
from Model import db, Car, CarSchema

cars_schema = CarSchema(many=True)
car_schema = CarSchema()

class CarResource(Resource):
    def get(self):
        cars = Car.query.all()
        cars = cars_schema.dump(cars).data
        return {'status': 'success', 'data': cars}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_schema.load(json_data)
        if errors:
            return errors, 422
        car = Car.query.filter_by(vin=data['vin']).first()
        if car:
            return {'message': 'Car already exists'}, 400
        car = Car(
            vin=data['vin'],
            car_model_id=data['car_model_id'],
            year=data['year'],
            )

        db.session.add(car)
        db.session.commit()

        result = car_schema.dump(car).data

        return { "status": 'success', 'data': result }, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_schema.load(json_data)
        if errors:
            return errors, 422
        car = Car.query.filter_by(id=data['id']).first()
        if not car:
            return {'message': 'Car does not exist'}, 400
        car.name = data['name']
        db.session.commit()

        result = car_schema.dump(car).data

        return { "status": 'success', 'data': result }, 204

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_schema.load(json_data)
        if errors:
            return errors, 422
        car = Car.query.filter_by(id=data['id']).delete()
        db.session.commit()

        result = car_schema.dump(car).data

        return { "status": 'success', 'data': result}, 204
