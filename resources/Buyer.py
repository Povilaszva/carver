from flask import request
from flask_restful import Resource
from Model import db, Buyer, BuyerSchema

buyers_schema = BuyerSchema(many=True)
buyer_schema = BuyerSchema()

class BuyerResource(Resource):
    def get(self):
        buyers = Buyer.query.all()
        buyers = buyers_schema.dump(buyers).data
        return {'status': 'success', 'data': buyers}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = buyer_schema.load(json_data)
        if errors:
            return errors, 422
        buyer = Buyer.query.filter_by(name=data['name']).first()
        if buyer:
            return {'message': 'Buyer already exists'}, 400
        buyer = Buyer(
            name=json_data['name']
            )

        db.session.add(buyer)
        db.session.commit()

        result = buyer_schema.dump(buyer).data

        return { "status": 'success', 'data': result }, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = buyer_schema.load(json_data)
        if errors:
            return errors, 422
        buyer = Buyer.query.filter_by(id=data['id']).first()
        if not buyer:
            return {'message': 'Buyer does not exist'}, 400
        buyer.name = data['name']
        db.session.commit()

        result = buyer_schema.dump(buyer).data

        return { "status": 'success', 'data': result }, 204

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = buyer_schema.load(json_data)
        if errors:
            return errors, 422
        buyer = Buyer.query.filter_by(id=data['id']).delete()
        db.session.commit()

        result = buyer_schema.dump(buyer).data

        return { "status": 'success', 'data': result}, 204
