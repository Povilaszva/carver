from flask import request
from flask_restful import Resource
from Model import db, CarModel, CarModelSchema, Make

car_models_schema = CarModelSchema(many=True)
car_model_schema = CarModelSchema()

class CarModelResource(Resource):
    def get(self):
        car_models = CarModel.query.all()
        car_models = car_models_schema.dump(car_models).data
        return {'status': 'success', 'data': car_models}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_model_schema.load(json_data)
        if errors:
            return {"status": "error", "data": errors}, 422
        print(data['make_id'])
        make_id = Make.query.filter_by(id=data['make_id']).first()
        if not make_id:
            return {'status': 'error', 'message': 'make not found'}, 400
        print("zdrw")
        print(CarModel)
        car_model = CarModel(
            make_id=data['make_id'],
            name=data['name']
            )
        print(car_model)
        db.session.add(car_model)
        db.session.commit()

        result = car_model_schema.dump(car_model).data

        return {'status': "success", 'data': result}, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_model_schema.load(json_data)
        if errors:
            return errors, 422
        car_model = CarModel.query.filter_by(id=data['id']).first()
        if not car_model:
            return {'message': 'Car Model does not exist'}, 400
        car_model.name = data['name']
        db.session.commit()

        result = car_model_schema.dump(car_model).data

        return { "status": 'success', 'data': result }, 204

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = car_model_schema.load(json_data)
        if errors:
            return errors, 422
        car_model = CarModel.query.filter_by(id=data['id']).delete()
        db.session.commit()

        result = car_model_schema.dump(car_model).data

        return { "status": 'success', 'data': result}, 204
