from flask import request
from flask_restful import Resource
from Model import db, Make, MakeSchema

makes_schema = MakeSchema(many=True)
make_schema = MakeSchema()

class MakeResource(Resource):
    def get(self):
        makes = Make.query.all()
        makes = makes_schema.dump(makes).data
        return {'status': 'success', 'data': makes}, 200

    def post(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = make_schema.load(json_data)
        if errors:
            return errors, 422
        make = Make.query.filter_by(name=data['name']).first()
        if make:
            return {'message': 'Make already exists'}, 400
        make = Make(
            name=json_data['name']
            )

        db.session.add(make)
        db.session.commit()

        result = make_schema.dump(make).data

        return { "status": 'success', 'data': result }, 201

    def put(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = make_schema.load(json_data)
        if errors:
            return errors, 422
        make = Make.query.filter_by(id=data['id']).first()
        if not make:
            return {'message': 'Make does not exist'}, 400
        make.name = data['name']
        db.session.commit()

        result = make_schema.dump(make).data

        return { "status": 'success', 'data': result }, 204

    def delete(self):
        json_data = request.get_json(force=True)
        if not json_data:
               return {'message': 'No input data provided'}, 400
        # Validate and deserialize input
        data, errors = make_schema.load(json_data)
        if errors:
            return errors, 422
        make = Make.query.filter_by(id=data['id']).delete()
        db.session.commit()

        result = make_schema.dump(make).data

        return { "status": 'success', 'data': result}, 204
