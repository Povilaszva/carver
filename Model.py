from flask import Flask
from marshmallow import Schema, fields, pre_load, validate
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy


ma = Marshmallow()
db = SQLAlchemy()


class Car(db.Model):
    __tablename__ = 'car'
    id = db.Column(db.Integer, primary_key=True)
    vin = db.Column(db.String(150), unique=True, nullable=False)
    year = db.Column(db.Date)
    car_model_id = db.Column(db.Integer, db.ForeignKey('car_model.id', ondelete='CASCADE'), nullable=False)
    car_model = db.relationship('CarModel', backref=db.backref('car', lazy='dynamic' ))

    def __init__(self, vin, car_model_id, year):
        self.vin = vin
        self.car_model_id = car_model_id
        self.year = year

class CarModel(db.Model):
    __tablename__ = 'car_model'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True, nullable=False)
    make_id = db.Column(db.Integer, db.ForeignKey('make.id', ondelete='CASCADE'), nullable=False)
    make = db.relationship('Make', backref=db.backref('car_model', lazy='dynamic' ))

    def __init__(self, name, make_id):
        self.name = name
        self.make_id = make_id


class Make(db.Model):
    __tablename__ = 'make'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True, nullable=False)

    def __init__(self, name):
        self.name = name

class Buyer(db.Model):
    __tablename__ = 'buyer'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True, nullable=False)

    def __init__(self, name):
        self.name = name

class SaleOrder(db.Model):
    __tablename__ = 'sale_order'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), unique=True, nullable=False)
    date_sold = db.Column(db.Date)
    car_id = db.Column(db.Integer, db.ForeignKey('car.id', ondelete='CASCADE'), nullable=False)
    car = db.relationship('Car', backref=db.backref('sale_order', lazy='dynamic' ))
    buyer_id = db.Column(db.Integer, db.ForeignKey('buyer.id', ondelete='CASCADE'), nullable=False)
    buyer = db.relationship('Buyer', backref=db.backref('sale_order', lazy='dynamic' ))

    def __init__(self, name, date_sold, car_id, buyer_id):
        self.name = name
        self.date_sold = date_sold
        self.car_id = car_id
        self.buyer_id = buyer_id

class CarModelSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)
    make_id = fields.Integer(required=True)


class CarSchema(ma.Schema):
    id = fields.Integer()
    vin = fields.String(required=True)
    year = fields.Date(required=True)
    car_model_id = fields.Integer(required=True)


class MakeSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)


class BuyerSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)


class SaleOrderSchema(ma.Schema):
    id = fields.Integer()
    name = fields.String(required=True)
    date_sold = fields.Date(required=True)
    car_id = fields.Integer(required=True)
    buyer_id = fields.Integer(required=True)
