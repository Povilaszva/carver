from flask import Blueprint
from flask_restful import Api
from resources.Make import MakeResource
from resources.CarModel import CarModelResource
from resources.Car import CarResource
from resources.Buyer import BuyerResource
from resources.SaleOrder import SaleOrderResource


api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Routes
api.add_resource(MakeResource, '/make')
api.add_resource(CarModelResource, '/carmodel')
api.add_resource(CarResource, '/car')
api.add_resource(SaleOrderResource, '/sale')
api.add_resource(BuyerResource, '/buyer')
